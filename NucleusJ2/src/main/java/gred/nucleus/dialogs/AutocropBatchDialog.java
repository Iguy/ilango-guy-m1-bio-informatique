package gred.nucleus.dialogs;

import gred.nucleus.autocrop.AutocropParameters;
import gred.nucleus.mainsNucelusJ.AutoCropCalling;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Guy ILANGO
 *
 * Class to run the Autocrop with default parameter
 *
 */

public class AutocropBatchDialog extends JFrame
{
    private static final long serialVersionUID = 1L;
    /**
     * initialization of variable
     */
    /**
     * JButton
     */
    private JButton _jButtonWorkDirectory = new JButton("Output Directory");
    private JButton _jButtonStart = new JButton("Start");
    private JButton _jButtonQuit = new JButton("Quit");
    private JButton _jButtonRawData = new JButton("Raw Data");
    private JButton _jButtonxVoxel = new JButton("X Voxel");
    private JButton _jButtonyVoxel = new JButton("Y voxel");
    private JButton _jButtonzSlice = new JButton("Z Slice");


    /**
     * Container
     */
    private Container _container;
    @SuppressWarnings("rawtypes")

    /**
     * JMenu
     */
    private JMenuBar menubar = new JMenuBar();
    private JMenu chooseautocrop = new JMenu("Change autocrop");
    private JMenuItem autoocropadvanced = new JMenuItem("Autocrop Advanced");


    /**
     * JTextField
     */

    private JTextField _jTextFieldWorkDirectory  =  new JTextField();
    private JTextField _jTextFieldRawData = new JTextField();



    /**
     * JLabel
     */

    private JLabel _jLabelWorkDirectory;



    /**
     * String
     */
    private String _workDirectory;
    private String _rawDataDirectory;


    private boolean _start = false;


    /**
     *
     * @param args
     */
    public static void main(String[] args)
    {
        AutocropBatchDialog autocropBatchDialog = new AutocropBatchDialog();
        autocropBatchDialog.setLocationRelativeTo(null);
    }


    /**
     *     Architecture of the graphical windows
     */
    public AutocropBatchDialog () {

        this.setTitle("Autocrop (batch)");
        this.setSize(500, 300);
        this.setLocationRelativeTo(null);
        _container = getContentPane();
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.1};
        gridBagLayout.rowHeights = new int[]{17, 71, 124, 7};
        gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.1};
        gridBagLayout.columnWidths = new int[]{236, 109, 72, 20};
        _container.setLayout(gridBagLayout);
        _container.add(menubar
                ,
                new GridBagConstraints
                        (
                                0, 1, 0, 0, 0.0, 0.0,
                                GridBagConstraints.NORTHWEST,
                                GridBagConstraints.NONE,
                                new Insets(-20, 330, 0, 0), 0, 0
                        )
        );
        menubar.add(chooseautocrop);

        chooseautocrop.add(autoocropadvanced);
        _jLabelWorkDirectory = new JLabel();
        _container.add
                (
                        _jLabelWorkDirectory,
                        new GridBagConstraints
                                (
                                        0, 1, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(0, 10, 0, 0), 0, 0
                                )
                );
        _jLabelWorkDirectory.setText("Work directory and Raw data choice : ");

        _container.add
                (
                        _jButtonRawData,
                        new GridBagConstraints
                                (
                                        0, 1, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(30, 10, 0, 0), 0, 0
                                )
                );
        _jButtonRawData.setPreferredSize(new java.awt.Dimension(120, 21));
        _jButtonRawData.setFont(new java.awt.Font("Albertus", 2, 10));

        _container.add
                (
                        _jTextFieldRawData,
                        new GridBagConstraints
                                (
                                        0, 1, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(30, 160, 0, 0), 0, 0
                                )
                );
        _jTextFieldRawData.setPreferredSize(new java.awt.Dimension(280, 21));
        _jTextFieldRawData.setFont(new java.awt.Font("Albertus", 2, 10));

        _container.add
                (
                        _jButtonWorkDirectory,
                        new GridBagConstraints
                                (
                                        0, 1, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(60, 10, 0, 0), 0, 0
                                )
                );
        _jButtonWorkDirectory.setPreferredSize(new java.awt.Dimension(120, 21));
        _jButtonWorkDirectory.setFont(new java.awt.Font("Albertus", 2, 10));

        _container.add
                (
                        _jTextFieldWorkDirectory,
                        new GridBagConstraints
                                (
                                        0, 1, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(60, 160, 0, 0), 0, 0
                                )
                );
        _jTextFieldWorkDirectory.setPreferredSize(new java.awt.Dimension(280, 21));
        _jTextFieldWorkDirectory.setFont(new java.awt.Font("Albertus", 2, 10));



        _container.setLayout(gridBagLayout);


        _container.add
                (
                        _jButtonStart,
                        new GridBagConstraints
                                (
                                        0, 2, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(70, 230, 0, 0), 0, 0
                                )
                );
        _jButtonStart.setPreferredSize(new java.awt.Dimension(120, 21));
        _container.add
                (
                        _jButtonQuit,
                        new GridBagConstraints
                                (
                                        0, 2, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST
                                        , GridBagConstraints.NONE,
                                        new Insets(70, 100, 0, 0), 0, 0
                                )
                );
        _jButtonQuit.setPreferredSize(new java.awt.Dimension(120, 21));
        this.setVisible(true);


        /**
         * Specific Listener
         */



        /**
         * Listener to change to the autocrop with all parameters
         */
        AutocropBatchDialog.AutocropAdvancedListener autocropAdvancedListener = new AutocropBatchDialog.AutocropAdvancedListener(this);
        autoocropadvanced.addActionListener(autocropAdvancedListener);

        /**
         * WorkDirectory
         */
        AutocropBatchDialog.WorkDirectoryListener wdListener = new AutocropBatchDialog.WorkDirectoryListener();
        _jButtonWorkDirectory.addActionListener(wdListener);

        /**
         * RawDataDirectory
         */
        AutocropBatchDialog.RAwDataDirectoryListener ddListener = new AutocropBatchDialog.RAwDataDirectoryListener();
        _jButtonRawData.addActionListener(ddListener);

        /**
         * Quit button
         */
        AutocropBatchDialog.QuitListener quitListener = new AutocropBatchDialog.QuitListener(this);
        _jButtonQuit.addActionListener(quitListener);

        /**
         * Start Button
         */
        AutocropBatchDialog.StartListener startListener = new AutocropBatchDialog.StartListener(this);
        _jButtonStart.addActionListener(startListener);

    }


    /**
     * Getter
     * @return
     */


    /**
     * WorkDirectory
     * @return
     */
    public String getWorkDirectory(){return _jTextFieldWorkDirectory.getText();}

    /**
     * RawDataDirectory
     * @return
     */
    public String getRawDataDirectory(){return _jTextFieldRawData.getText();}

    /**
     * State Of start Button
     * @return
     */
    public boolean isStart() {	return _start; }


    /********************************************************************************************************************************************
     * 	Classes listener to interact with the several element of the window
     */
    /********************************************************************************************************************************************
     /********************************************************************************************************************************************
     /********************************************************************************************************************************************
     /********************************************************************************************************************************************/





    /**
     * change to autocrop with all parameters
     */
    class AutocropAdvancedListener implements ActionListener{
        AutocropBatchDialog _autocropBatchDialog;
        /**
         * @param autoocropBatchDialog
         *
         */
        public AutocropAdvancedListener(AutocropBatchDialog autoocropBatchDialog) {
            _autocropBatchDialog = autoocropBatchDialog;
        }
        public void actionPerformed(ActionEvent actionEvent)
        {
            new AutocropBatchDialogAdvanced();
            _autocropBatchDialog.dispose();
        }
    }

    /**
     * Start Button
     */
    class StartListener implements ActionListener
    {
        AutocropBatchDialog _autocropBatchDialog;
        /**
         *
         * @param autoocropBatchDialog
         */
        public  StartListener (AutocropBatchDialog autoocropBatchDialog)
        {
            _autocropBatchDialog = autoocropBatchDialog;
        }
        /**
         *
         */
        public void actionPerformed(ActionEvent actionEvent)
        {
            if (_jTextFieldWorkDirectory.getText().isEmpty() || _jTextFieldRawData.getText().isEmpty())
                JOptionPane.showMessageDialog
                        (
                                null,
                                "You did not choose a work directory or the raw data",
                                "Error",
                                JOptionPane.ERROR_MESSAGE
                        );
            else
            {
                _start=true;
                _autocropBatchDialog.dispose();
                try{
                    AutocropParameters autocropParameters = new AutocropParameters
                            (

                                    getRawDataDirectory(),
                                    getWorkDirectory()

                            );

                    AutoCropCalling autoCrop = new AutoCropCalling(autocropParameters);
                    autoCrop.run(); }catch (Exception e){e.printStackTrace();}
            }
        }
    }



    /**
     *Quit Button
     *
     */

    class QuitListener implements ActionListener
    {
        AutocropBatchDialog _autocropBatchDialog;
        /**
         *
         * @param autoocropBatchDialog
         */
        public  QuitListener (AutocropBatchDialog autoocropBatchDialog)
        {
            _autocropBatchDialog = autoocropBatchDialog;
        }

        public void actionPerformed(ActionEvent actionEvent)
        {
            _autocropBatchDialog.dispose();
        }
    }

    /**
     * WorkDirectory button
     */
    class WorkDirectoryListener implements ActionListener
    {

        public void actionPerformed(ActionEvent actionEvent)
        {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnValue = jFileChooser.showOpenDialog(getParent());
            if(returnValue == JFileChooser.APPROVE_OPTION)
            {
                @SuppressWarnings("unused")
                String run = jFileChooser.getSelectedFile().getName();
                _workDirectory = jFileChooser.getSelectedFile().getAbsolutePath();
                _jTextFieldWorkDirectory.setText(_workDirectory);
            }
            setCursor (Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }
    /**
     *RawDataDirectory button
     *
     */
    class RAwDataDirectoryListener implements ActionListener
    {

        /**
         *
         */
        public void actionPerformed(ActionEvent actionEvent)
        {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnValue = jFileChooser.showOpenDialog(getParent());
            if(returnValue == JFileChooser.APPROVE_OPTION)
            {
                @SuppressWarnings("unused")
                String run = jFileChooser.getSelectedFile().getName();
                _rawDataDirectory = jFileChooser.getSelectedFile().getAbsolutePath();
                _jTextFieldRawData.setText(_rawDataDirectory);
            }
            setCursor (Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }    }
}
