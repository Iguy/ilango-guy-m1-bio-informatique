package gred.nucleus.dialogs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Guy Ilango
 */

public class NucleusJMainMenu extends JFrame// implements PlugIn
{
    /**
     * initialization of variables
     */

    JPanel main_menu = new JPanel();
    JButton autocrop = new JButton("Autocrop");
    JButton segmentation = new JButton("Segmentation");




    public NucleusJMainMenu() {
        /**
         * Creating the window
         */
        this.setTitle("NucleusJ V2");
        this.setSize(400, 250);
        Container contenu = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT); // ou VERTICAL_SPLIT

       main_menu.setBorder(BorderFactory.createTitledBorder("Main Menu"));

        autocrop.setPreferredSize(new Dimension(300, 60));
        segmentation.setPreferredSize(new Dimension(300, 60));

        main_menu.add(autocrop);
        main_menu.add(segmentation);









        main_menu.setLayout(new GridLayout(3, 3));


        contenu.add(main_menu);

        this.getContentPane().add(contenu);


        this.setVisible(true);

/***************************************************
 * add listener for each buttons
 ***************************************************/




        /**
         * Segmentation
         */
        segmentation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {


                new NewSegmentationBatchDialog(); //call  another class
            }
        });


        /**
         * Autocrop
         */
        autocrop.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                new AutocropChooserDialog();

            }
        });
    }


}