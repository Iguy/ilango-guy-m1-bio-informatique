package gred.nucleus.dialogs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Guy ILANGO
 * Class to choose the autocrop mode
 */
public class AutocropChooserDialog extends JFrame {
    public AutocropChooserDialog(){
        /**
         * Initialization of variables
         */

        /**
         * JButton
         */
        JButton autocrop = new JButton("Autocrop");
        JButton autocropadvanced = new JButton("Autocrop Advanced");
        autocrop.setToolTipText("Autocrop with default parameter");
        autocropadvanced.setToolTipText("Autocrop with all parameter define by the user");


        /**
         * JFrame
         */
        final JFrame container = new JFrame();
        container.setTitle("Autocrop Chooser");
        container.setSize(450,100);
        container.add(autocrop);
        container.add(autocropadvanced);
        container.setLayout(new GridBagLayout());
        container.setVisible(true);

/**
 * Anonyme Listener
 *
 */

        /**
         * Open window for Autocrop With default parameters
         */
        autocrop.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                new AutocropBatchDialog();
                container.dispose();
            }
    });

        /**
         * Open window for Autocrop With all parameters
         */
        autocropadvanced.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                new AutocropBatchDialogAdvanced();
                container.dispose();
            }
    });

    }
    public static void main(String[] args){
        new AutocropChooserDialog();
    }
}
