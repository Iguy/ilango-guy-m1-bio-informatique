package gred.nucleus.dialogs;

import gred.nucleus.autocrop.AutocropParameters;
import gred.nucleus.mainsNucelusJ.AutoCropCalling;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Guy ILANGO
 *
 * Class to run the autocrop with all parameter
 */

public class AutocropBatchDialogAdvanced extends JFrame
{
    private static final long serialVersionUID = 1L;
    /**
     * Initialization of variables
     */

    /**
     * JButton
     */
    private JButton _jButtonWorkDirectory = new JButton("Output Directory");
    private JButton _jButtonStart = new JButton("Start");
    private JButton _jButtonQuit = new JButton("Quit");
    private JButton _jButtonRawData = new JButton("Raw Data");


    /**
     * JMenu
     */
    private JMenuBar menubar = new JMenuBar();
    private JMenu chooseautocrop = new JMenu("Change autocrop");
    private JMenuItem autoocrop = new JMenuItem("Autocrop");


    /**
     * Container
     */
    private Container _container;
    @SuppressWarnings("rawtypes")

    /**
     * JFormattedTextField
     */
    private JFormattedTextField _jTextFieldXCalibration = new JFormattedTextField(Number.class);
    private JFormattedTextField _jTextFieldYCalibration = new JFormattedTextField(Number.class);
    private JFormattedTextField _jTextFieldZCalibration =  new JFormattedTextField(Number.class);
    private JFormattedTextField _jTextFieldThreshold =  new JFormattedTextField(Number.class);
    private JFormattedTextField _jTextFieldDefault =  new JFormattedTextField(Number.class);
    private JFormattedTextField _jTextFieldxVoxel =  new JFormattedTextField(Number.class);
    private JFormattedTextField _jTextFieldyVoxel =  new JFormattedTextField(Number.class);
    private JFormattedTextField _jTextFieldzSlice =  new JFormattedTextField(Number.class);
    private JFormattedTextField _jTextFieldnumDefailt =  new JFormattedTextField(Number.class);

    /**
     * JTextField
     */
    private JTextField _jTextFieldUnit =  new JTextField();
    private JTextField _jTextFieldWorkDirectory  =  new JTextField();
    private JTextField _jTextFieldRawData = new JTextField();
    private JTextField _jTextFieldMin = new JTextField();
    private JTextField _jTextFieldMax = new JTextField();

    /**
     * JLabel
     */
    private JLabel _jLabelXcalibration;
    private JLabel _jLabelYcalibration;
    private JLabel _jLabelZcalibration;
    private JLabel _jLabelThresh;
    private JLabel _jLabelOTSU;
    private JLabel _jLabelDefault;
    private JLabel _jLabelThreshold;
    private JLabel _jLabelWorkDirectory;
    private JLabel  _jLabelCalibration;
    private JLabel  _jLabelVoxel;
    private JLabel  _jLabelxVoxel;
    private JLabel  _jLabelyVoxel;
    private JLabel  _jLabelzSlice;
    private JLabel _jLabelnumThreshold;
    private JLabel _jLabelnumDefault;
    private JLabel _jLabelSegmentation;
    private JLabel _jLabelVolumeMin;
    private JLabel _jLabelVolumeMax;


    /**
     * String
     */
    private String _workDirectory;
    private String _rawDataDirectory;


    private boolean _start = false;


    /**
     *
     * @param args
     */
    public static void main(String[] args)
    {
        AutocropBatchDialogAdvanced autocropBatchDialogAdvanced = new AutocropBatchDialogAdvanced();
        autocropBatchDialogAdvanced.setLocationRelativeTo(null);
    }


    /**
     *     Architecture of the graphical windows
     */
    public AutocropBatchDialogAdvanced ()
    {

        this.setTitle("Autocrop (batch)");
        this.setSize(460, 800);
        this.setLocationRelativeTo(null);
        _container = getContentPane();
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 0.1};
        gridBagLayout.rowHeights = new int[] {17, 71, 124, 7};
        gridBagLayout.columnWeights = new double[] {0.0, 0.0, 0.0, 0.1};
        gridBagLayout.columnWidths = new int[] {236, 109, 72, 20};
        _container.setLayout (gridBagLayout);
        _container.add(menubar
                ,
                new GridBagConstraints
                        (
                                0, 1, 0, 0, 0.0, 0.0,
                                GridBagConstraints.NORTHWEST,
                                GridBagConstraints.NONE,
                                new Insets(-20, 330, 0, 0), 0, 0
                        )
        );
        menubar.add(chooseautocrop);
        chooseautocrop.add(autoocrop);

        _jLabelWorkDirectory = new JLabel();
        _container.add
                (
                        _jLabelWorkDirectory,
                        new GridBagConstraints
                                (
                                        0, 1, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(0, 10, 0, 0), 0, 0
                                )
                );
        _jLabelWorkDirectory.setText("Work directory and Raw data choice : ");

        _container.add
                (
                        _jButtonRawData,
                        new GridBagConstraints
                                (
                                        0, 1, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(30, 10, 0, 0), 0, 0
                                )
                );
        _jButtonRawData.setPreferredSize(new java.awt.Dimension(120, 21));
        _jButtonRawData.setFont(new java.awt.Font("Albertus",2,10));

        _container.add
                (
                        _jTextFieldRawData,
                        new GridBagConstraints
                                (
                                        0, 1, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(30, 160, 0, 0), 0, 0
                                )
                );
        _jTextFieldRawData.setPreferredSize(new java.awt.Dimension(280, 21));
        _jTextFieldRawData.setFont(new java.awt.Font("Albertus",2,10));

        _container.add
                (
                        _jButtonWorkDirectory,
                        new GridBagConstraints
                                (
                                        0, 1, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(60, 10, 0, 0), 0, 0
                                )
                );
        _jButtonWorkDirectory.setPreferredSize(new java.awt.Dimension(120, 21));
        _jButtonWorkDirectory.setFont(new java.awt.Font("Albertus",2,10));

        _container.add
                (
                        _jTextFieldWorkDirectory,
                        new GridBagConstraints
                                (
                                        0, 1, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(60, 160, 0, 0), 0, 0
                                )
                );
        _jTextFieldWorkDirectory.setPreferredSize(new java.awt.Dimension(280, 21));
        _jTextFieldWorkDirectory.setFont(new java.awt.Font("Albertus",2,10));
        _jLabelCalibration = new JLabel();
        _container.add
                (
                        _jLabelCalibration,
                        new GridBagConstraints
                                (
                                        0, 2, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(30, 10, 0, 0), 0, 0
                                )
                );
        _jLabelCalibration.setText("Image Calibration : ");


        _container.setLayout (gridBagLayout);
        _jLabelXcalibration = new JLabel();
        _container.add
                (
                        _jLabelXcalibration,
                        new GridBagConstraints
                                (
                                        0, 2, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(55, 20, 0, 0), 0, 0
                                )
                );
        _jLabelXcalibration.setText("x :");
        _jLabelXcalibration.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));
        _container.add
                (
                        _jTextFieldXCalibration,
                        new GridBagConstraints
                                (
                                        0, 2, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(55, 80, 0, 0), 0, 0
                                )
                );
        _jTextFieldXCalibration.setText("40");
        _jTextFieldXCalibration.setPreferredSize(new java.awt.Dimension(60, 21));

        _jLabelYcalibration = new JLabel();
        _container.add
                (
                        _jLabelYcalibration,
                        new GridBagConstraints
                                (
                                        0, 2, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(90, 20, 0, 0), 0, 0
                                )
                );
        _jLabelYcalibration.setText("y :");
        _jLabelYcalibration.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));
        _container.add
                (
                        _jTextFieldYCalibration,
                        new GridBagConstraints
                                (
                                        0, 2, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(90, 80, 0, 0), 0, 0
                                )
                );
        _jTextFieldYCalibration.setText("40");
        _jTextFieldYCalibration.setPreferredSize(new java.awt.Dimension(60, 21));

        _jLabelZcalibration = new JLabel();
        _container.add
                (
                        _jLabelZcalibration,
                        new GridBagConstraints
                                (
                                        0, 2, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(125, 20, 0, 0), 0, 0
                                )
                );
        _jLabelZcalibration.setText("z :");
        _jLabelZcalibration.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));
        _container.add
                (
                        _jTextFieldZCalibration,
                        new GridBagConstraints
                                (
                                        0, 2,0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(125, 80, 0, 0), 0, 0
                                )
                );
        _jTextFieldZCalibration.setText("20");
        _jTextFieldZCalibration.setPreferredSize(new java.awt.Dimension(60, 21));

        _jLabelSegmentation = new JLabel();
        _container.add
                (
                        _jLabelSegmentation,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(40, 10, 0, 0), 0, 0
                                )
                );
        _jLabelSegmentation.setText("Choose the min and max volumes of the nucleus:");

        _jLabelVolumeMin = new JLabel();
        _container.add
                (
                        _jLabelVolumeMin,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(75, 10, 0, 0), 0, 0
                                )
                );
        _jLabelVolumeMin.setText("Minimun volume of the segmented nucleus :");
        _jLabelVolumeMin.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));
        _container.add
                (
                        _jTextFieldMin,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(75, 320, 0, 0), 0, 0
                                )
                );
        _jTextFieldMin.setText("1");
        _jTextFieldMin.setPreferredSize(new java.awt.Dimension( 60, 21));

        _jLabelVolumeMax = new JLabel();
        _container.add
                (
                        _jLabelVolumeMax,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(110, 10, 0, 0), 0, 0
                                )
                );
        _jLabelVolumeMax.setText("Maximum volume of the segmented nucleus :");
        _jLabelVolumeMax.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));
        _container.add
                (
                        _jTextFieldMax,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(110, 320, 0, 0), 0, 0
                                )
                );
        _jTextFieldMax.setText("1000000000");
        _jTextFieldMax.setPreferredSize(new java.awt.Dimension (60, 21));
        _jLabelVoxel = new JLabel();
        _container.add
                (
                        _jLabelVoxel,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(145, 10, 0, 0), 0, 0
                                )
                );
        _jLabelVoxel.setText("Number of voxel around box");
        _jLabelxVoxel = new JLabel();
        _container.add
                (
                        _jLabelxVoxel,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(180, 10, 0, 0), 0, 0
                                )
                );
        _jLabelxVoxel.setText("X Voxel:");
        _jLabelxVoxel.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));

        _container.add
                (
                        _jTextFieldxVoxel,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(180, 80, 0, 0), 0, 0
                                )
                );
        _jTextFieldxVoxel.setText("40");
        _jTextFieldxVoxel.setPreferredSize(new java.awt.Dimension(65, 20));
        _jLabelyVoxel = new JLabel();
        _container.add
                (
                        _jLabelyVoxel,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(215, 10, 0, 0), 0, 0
                                )
                );
        _jLabelyVoxel.setText("Y Voxel:");
        _jLabelyVoxel.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));

        _container.add
                (
                        _jTextFieldyVoxel,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(215, 80, 0, 0), 0, 0
                                )
                );
        _jTextFieldyVoxel.setText("40");
        _jTextFieldyVoxel.setPreferredSize(new java.awt.Dimension(65, 20));_jLabelyVoxel = new JLabel();

        _jLabelzSlice = new JLabel();
        _container.add
                (
                        _jLabelzSlice,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(250, 10, 0, 0), 0, 0
                                )
                );
        _jLabelzSlice.setText("Z Slice:");
        _jLabelzSlice.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));

        _container.add
                (
                        _jTextFieldzSlice,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(250, 80, 0, 0), 0, 0
                                )
                );
        _jTextFieldzSlice.setText("20");
        _jTextFieldzSlice.setPreferredSize(new java.awt.Dimension(65, 20));



        _jLabelThresh = new JLabel();
        _container.add
                (
                        _jLabelThresh,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(285, 10, 0, 0), 0, 0
                                )
                );
        _jLabelThresh.setText("Number of Slice used to Start OSTU Thresholding");

        _jLabelDefault = new JLabel();
        _container.add
                (
                        _jLabelDefault,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(320, 10, 0, 0), 0, 0
                                )
                );
        _jLabelDefault.setText("Default:");
        _jLabelDefault.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));

        _container.add
                (
                        _jTextFieldDefault,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(320, 80, 0, 0), 0, 0
                                )
                );
        _jTextFieldDefault.setText("0");
        _jTextFieldDefault.setPreferredSize(new java.awt.Dimension( 60, 21));
        _jLabelOTSU = new JLabel();
        _container.add
                (
                        _jLabelOTSU,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(355, 10, 0, 0), 0, 0
                                )
                );
        _jLabelOTSU.setText("OTSU Threshold minimum");
        _jLabelThreshold = new JLabel();
        _container.add
                (
                        _jLabelThreshold,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(390, 10, 0, 0), 0, 0
                                )
                );
        _jLabelThreshold.setText("Threshold:");
        _jLabelThreshold.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));
        _container.add
                (
                        _jTextFieldThreshold,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(390, 80, 0, 0), 0, 0
                                )
                );
        _jTextFieldThreshold.setText("20");
        _jTextFieldThreshold.setPreferredSize(new java.awt.Dimension (60, 21));
        _jLabelnumThreshold = new JLabel();
        _container.add
                (
                        _jLabelnumThreshold,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(425, 10, 0, 0), 0, 0
                                )
                );
        _jLabelnumThreshold.setText("Channel's number used to compute OTSU Threshold:");

        _jLabelnumDefault = new JLabel();
        _container.add
                (
                        _jLabelnumDefault,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(460, 10, 0, 0), 0, 0

                                )

                );
        _jLabelnumDefault.setText("Default:");
        _jLabelnumDefault.setFont(new java.awt.Font("Albertus Extra Bold (W1)",2,12));
        _container.add
                (
                        _jTextFieldnumDefailt,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0, 0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(460, 80, 0, 0), 0, 0
                                )
                );
        _jTextFieldnumDefailt.setText("0");
        _jTextFieldnumDefailt.setPreferredSize(new java.awt.Dimension (60, 21));


        _container.add
                (
                        _jButtonStart,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0,0.0, 0.0,
                                        GridBagConstraints.NORTHWEST,
                                        GridBagConstraints.NONE,
                                        new Insets(520, 230, 0,0), 0, 0
                                )
                );
        _jButtonStart.setPreferredSize(new java.awt.Dimension(120, 21));
        _container.add
                (
                        _jButtonQuit,
                        new GridBagConstraints
                                (
                                        0, 3, 0, 0,0.0, 0.0,
                                        GridBagConstraints.NORTHWEST
                                        ,GridBagConstraints.NONE,
                                        new Insets(520, 100, 0, 0), 0, 0
                                )
                );
        _jButtonQuit.setPreferredSize(new java.awt.Dimension(120, 21));
        this.setVisible(true);

        /**
         * Specific Listener
         */

        /**
         * Listener to change to the default parameters autocrop
         */
        AutocropBatchDialogAdvanced.AutocropListener autocropListener = new AutocropBatchDialogAdvanced.AutocropListener(this);
        autoocrop.addActionListener(autocropListener);



        /**
         * WorkDirectory
         */
        WorkDirectoryListener wdListener = new WorkDirectoryListener();
        _jButtonWorkDirectory.addActionListener(wdListener);

        /**
         * RawDataDirectory
         */
        RAwDataDirectoryListener ddListener = new RAwDataDirectoryListener();
        _jButtonRawData.addActionListener(ddListener);
        /**
         * Quit button
         */
        QuitListener quitListener = new QuitListener(this);
        _jButtonQuit.addActionListener(quitListener);
        /**
         * Start Button
         */
        StartListener startListener = new StartListener(this);
        _jButtonStart.addActionListener(startListener);

    }

    /**
     * Getter
     * @return
     */

    /**
     * Xcalibration
     * @return
     */
    public double getXCalibration()
    {
        String xCal = _jTextFieldXCalibration.getText();
        return Double.parseDouble(xCal.replaceAll(",", "."));
    }

    /**
     * Ycalibration
     * @return
     */
    public double getYCalibration()
    {
        String yCal = _jTextFieldYCalibration.getText();
        return Double.parseDouble(yCal.replaceAll(",", "."));
    }

    /**
     * Zcalibration
     * @return
     */
    public double getZCalibration()
    {
        String zCal = _jTextFieldZCalibration.getText();
        return Double.parseDouble(zCal.replaceAll(",", "."));
    }

    /**
     * Xvoxel
     * @return
     */
    public int getXvoxel(){ return Integer.parseInt(_jTextFieldxVoxel.getText().replaceAll(",","."));}

    /**
     * Yvoxel
     * @return
     */
    public int getYvoxel(){ return Integer.parseInt(_jTextFieldyVoxel.getText().replaceAll(",","."));}

    /**
     * Zslices
     * @return
     */
    public int getZvoxel(){ return Integer.parseInt(_jTextFieldzSlice.getText().replaceAll(",","."));}

    /**
     * Threshold
     * @return
     */
    public int getThreshold(){ return Integer.parseInt(_jTextFieldThreshold.getText().replaceAll(",","."));}

    /**
     * minimum Volume
     * @return
     */
    public int getVMin(){ return Integer.parseInt(_jTextFieldMin.getText().replaceAll(",","."));}

    /**
     * Maximum Volume
     * @return
     */
    public int getVMax(){ return Integer.parseInt(_jTextFieldMax.getText().replaceAll(",","."));}

    /**
     * Number of channels
     * @return
     */
    public int getChannel(){ return Integer.parseInt(_jTextFieldnumDefailt.getText().replaceAll(",","."));}

    /**
     * Number of slice to start OTSU thresholding
     * @return
     */
    public int getDefault(){ return Integer.parseInt(_jTextFieldDefault.getText().replaceAll(",",".")); }

    /**
     * WorkDirectory
     * @return
     */
    public String getWorkDirectory(){return _jTextFieldWorkDirectory.getText();}

    /**
     *RawDataDirectory
     * @return
     */
    public String getRawDataDirectory(){return _jTextFieldRawData.getText();}

    /**
     * State of start button
     * @return
     */
    public boolean isStart() {	return _start; }


    /********************************************************************************************************************************************
     * 	Classes listener to interact with the several element of the window
     */
    /********************************************************************************************************************************************
     /********************************************************************************************************************************************
     /********************************************************************************************************************************************
     /********************************************************************************************************************************************/



    /**
     * change to autocrop with default parameters
     */
    class AutocropListener implements ActionListener{
        AutocropBatchDialogAdvanced _autocropBatchDialogAdvanced;
        /**
         * @param autoocropBatchDialogAdvanced
         *
         */
        public AutocropListener(AutocropBatchDialogAdvanced autoocropBatchDialogAdvanced) {
            _autocropBatchDialogAdvanced = autoocropBatchDialogAdvanced;
        }
        public void actionPerformed(ActionEvent actionEvent)
        {
            new AutocropBatchDialog();
            _autocropBatchDialogAdvanced.dispose();

        }
    }


    /**
     *Start button
     *
     */

    class StartListener implements ActionListener
    {
        AutocropBatchDialogAdvanced _autocropBatchDialogAdvanced;
        /**
         *
         * @param autoocropBatchDialogAdvanced
         */
        public  StartListener (AutocropBatchDialogAdvanced autoocropBatchDialogAdvanced)
        {
            _autocropBatchDialogAdvanced = autoocropBatchDialogAdvanced;
        }

        public void actionPerformed(ActionEvent actionEvent)
        {
            if (_jTextFieldWorkDirectory.getText().isEmpty() || _jTextFieldRawData.getText().isEmpty())
                JOptionPane.showMessageDialog
                        (
                                null,
                                "You did not choose a work directory or the raw data",
                                "Error",
                                JOptionPane.ERROR_MESSAGE
                        );
            else
            {
                _start=true;
                _autocropBatchDialogAdvanced.dispose();

                /**
                 * Run the autorop
                 * @param:
                 * getRawDataDirectory(),
                 * getWorkDirectory(),
                 * getXCalibration(),
                 * getYCalibration(),
                 * getZCalibration(),
                 * getXvoxel(),
                 * getYvoxel(),
                 * getZvoxel(),
                 * getDefault(),
                 * getThreshold(),
                 * getChannel(),
                 * getVMin(),
                 * getVMax()
                 */
                try{
                    AutocropParameters autocropParameters = new AutocropParameters
                            (

                                    getRawDataDirectory(),
                                    getWorkDirectory(),
                                    getXCalibration(),
                                    getYCalibration(),
                                    getZCalibration(),
                                    getXvoxel(),
                                    getYvoxel(),
                                    getZvoxel(),
                                    getDefault(),
                                    getThreshold(),
                                    getChannel(),
                                    getVMin(),
                                    getVMax()
                            );

                    AutoCropCalling autoCrop = new AutoCropCalling(autocropParameters);
                    autoCrop.run(); }catch (Exception e){e.printStackTrace();}
            }
        }
    }

    /**
     *quit button
     *
     */

    class QuitListener implements ActionListener
    {
        AutocropBatchDialogAdvanced _autocropBatchDialogAdvanced;
        /**
         *
         * @param autoocropBatchDialogAdvanced
         */
        public  QuitListener (AutocropBatchDialogAdvanced autoocropBatchDialogAdvanced)
        {
            _autocropBatchDialogAdvanced = autoocropBatchDialogAdvanced;
        }


        public void actionPerformed(ActionEvent actionEvent)
        {
            _autocropBatchDialogAdvanced.dispose();
        }
    }
    /**
     *WorkDirectory button
     *
     */
    class WorkDirectoryListener implements ActionListener
    {

        public void actionPerformed(ActionEvent actionEvent)
        {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnValue = jFileChooser.showOpenDialog(getParent());
            if(returnValue == JFileChooser.APPROVE_OPTION)
            {
                @SuppressWarnings("unused")
                String run = jFileChooser.getSelectedFile().getName();
                _workDirectory = jFileChooser.getSelectedFile().getAbsolutePath();
                _jTextFieldWorkDirectory.setText(_workDirectory);
            }
            setCursor (Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }


    /**
     *RawDataDirectory button
     *
     */
    class RAwDataDirectoryListener implements ActionListener
    {

        public void actionPerformed(ActionEvent actionEvent)
        {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnValue = jFileChooser.showOpenDialog(getParent());
            if(returnValue == JFileChooser.APPROVE_OPTION)
            {
                @SuppressWarnings("unused")
                String run = jFileChooser.getSelectedFile().getName();
                _rawDataDirectory = jFileChooser.getSelectedFile().getAbsolutePath();
                _jTextFieldRawData.setText(_rawDataDirectory);
            }
            setCursor (Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }
}