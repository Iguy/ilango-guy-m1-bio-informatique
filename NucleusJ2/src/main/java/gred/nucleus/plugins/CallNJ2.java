package gred.nucleus.plugins;

import gred.nucleus.dialogs.NucleusJMainMenu;
import ij.plugin.PlugIn;

import javax.swing.*;

/**
 * @author Guy ILANGO
 * Class to call NJ2 Main Menu
 *
 */


public class CallNJ2 implements PlugIn {
    public void run(String args) {


/*public class CallNJ2  {
    public static void main(String[] args) {
*/

        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
        try {
            //here you can put the selected theme class name in JTattoo
            UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");

        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        new NucleusJMainMenu();   // Call NJMainMenu


    }
}

